def keep_lines_by_index(numpy_array, start_index, end_index, remove_last_column=False):
    if remove_last_column:
        return numpy_array[start_index: end_index, :-1]
    else:
        return numpy_array[start_index: end_index, :]


def filter_columns_by_index(numpy_array, index):
    return numpy_array[:, index]

