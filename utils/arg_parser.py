from argparse import ArgumentParser


def parse_arguments():

    parser = ArgumentParser(
        description="Parse main script's arguments"
    )

    parser.add_argument(
        '--source-file',
        dest="source_file",
        type=str,
        help='Path to CSV file',
        required=True
    )

    parser.add_argument(
        '--generate-report-source',
        dest="generate_report_source",
        type=str,
        help='If true, a html file containing data about the source dataset will be created at the given path',
        required=False
    )

    parser.add_argument(
        '--separator',
        dest="separator",
        type=str,
        help='Str used to separate data in the CSV file',
        required=False
    )

    parser.add_argument(
        '--training-portion',
        dest="training_portion",
        type=float,
        help='% of rows to be used as training data. 1-training_portion% of rows will then be used as guessing data',
        required=False
    )

    parser.add_argument(
        '--classifier',
        dest="name_of_classifier",
        type=str,
        help='Name of the classifier to use for this run',
        required=True
    )

    parser.add_argument(
        '--classifier-parameters',
        dest="classifier_parameters",
        type=str,
        help='List of arguments to be used with the specified classifier. Omitting this parameter will result in using'
             'default parameters contained in each classifier\'s file',
        required=False
    )

    parser.set_defaults(
        source_file="resources/antivirus_dataset.csv",
        separator=",",
        generate_report_source=None,
        training_portion=0.80,
        classifier_parameters=None
    )

    return parser.parse_args()


def parse_classifier_arguments():
    str_args = parse_arguments().classifier_parameters

    if str_args is None or str_args == "None":
        return None

    res = {}
    for param in str_args.split(','):
        res[param.split('=')[0]] = param.split('=')[1]
    return res if len(res) > 0 else None
