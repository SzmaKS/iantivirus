from sklearn import svm

from sklearn.metrics import classification_report, accuracy_score

from classifiers.LinearDiscriminantAnalysisClassifier import get_lda
from classifiers.MLPClassifier import get_mlp
from classifiers.KNeighborsClassifier import get_kn
from classifiers.QuadraticDiscriminantAnalysisClassifier import get_qda
from classifiers.RandomForestClassifier import get_rfc
from classifiers.SGDClassifier import get_sgd
from classifiers.SVCClassifier import get_svc

output_labels = ['legitimate', 'not legitimate']

classifier = None


def get_classifier(name_of_classifier=None, **kwargs):
    global classifier

    if classifier is not None:
        return classifier

    classifier_list = {
        "mlp": get_mlp,
        "svc": get_svc,
        "rfc": get_rfc,
        "kn": get_kn,
        "qda": get_qda,
        "lda": get_lda,
        "sgd": get_sgd,
    }

    func = classifier_list.get(name_of_classifier, None)

    if func is not None:
        classifier = func(**kwargs)
    else:
        print("Classifier unknown: selecting mlp as default classifier")
        classifier = get_classifier(
            name_of_classifier='mlp',
        )

    return classifier


def data_fit(data_columns, target_columns):
    return get_classifier().fit(data_columns, target_columns)


def predict(data_set):
    return get_classifier().predict(data_set)


def get_result(real, predicted):
    return classification_report(real, predicted, labels=[1, ])


def get_score(real, predicted):
    return accuracy_score(real, predicted)
