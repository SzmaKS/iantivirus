import numpy as np
import pandas as pd


def csv_reader(file_path, sep=',', engine='python') -> pd.DataFrame:
    df = pd.read_csv(file_path, sep=sep, engine=engine)
    return df


def filter_columns(data_frame, columns_to_filter) -> pd.DataFrame:
    for column in data_frame:
        if (data_frame[column].dtype == np.float64 or data_frame[column].dtype == np.int64) and not (column in columns_to_filter):
            continue
        else:
            data_frame = data_frame.drop(column, axis=1, inplace=False)

    return data_frame


def shuffle(numpy_array):
    np.random.shuffle(numpy_array)
    return numpy_array
