from sklearn.discriminant_analysis import LinearDiscriminantAnalysis


def get_lda(**kwargs):
    return LinearDiscriminantAnalysis(**kwargs) if len(kwargs) > 0 else LinearDiscriminantAnalysis(
        n_components=None,
        priors=None,
        shrinkage=None,
        solver='svd',
        store_covariance=True,
        tol=0.0000001
    )
