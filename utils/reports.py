import pandas_profiling


def generate_report(data_frame, title='Profiling report', save_as_file=''):
    profile = data_frame.profile_report(title=title)

    if save_as_file != '':
        profile.to_file(output_file=save_as_file)

    return profile
