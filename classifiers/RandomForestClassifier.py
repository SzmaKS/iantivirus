from sklearn.ensemble import RandomForestClassifier


def get_rfc(**kwargs):
    return RandomForestClassifier(**kwargs) if len(kwargs) > 0 else RandomForestClassifier(
        max_depth=35,
        n_estimators=70,
        max_features=3
    )
