from sklearn.linear_model import SGDClassifier


def get_sgd(**kwargs):
    return SGDClassifier(**kwargs) if len(kwargs) > 0 else SGDClassifier(
        alpha=0.0001,
        average=False, class_weight=None,
        early_stopping=False,
        epsilon=0.01,
        eta0=0.0,
        fit_intercept=True,
        l1_ratio=0.18,
        learning_rate='optimal',
        loss='hinge',
        max_iter=5000,
        n_iter_no_change=10,
        n_jobs=None,
        penalty='l2',
        power_t=0.6,
        random_state=None,
        shuffle=True,
        tol=0.001,
        validation_fraction=0.05,
        verbose=0,
        warm_start=False
    )
