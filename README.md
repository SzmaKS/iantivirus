# IAntivirus

Script qui teste l'efficacité de la prédiction d'algorithmes sur un dataset sous forme de CSV.

# Lancement

Exécutez simplement `main.py` avec python3.7.4  
Le script va se lancer avec une liste prédéfinie de classifiers    

Si vous souhaiter effectuer un run avec des paramètres personnalisés, vous pouvez appeler ` analyzr.py` avec les paramètres suivants:
* `--classifier=` Nom du classifier à utiliser
* `--classifier-parameters` Liste de paramètres à utiliser avec le classifier. /!\ Ils doivent correspondre au classifier (i.e être valides), et ne pas contenir d'espace /!\
* `--source` Chemin vers le dataset
* `--separator` Séparateur du .CSV utilisé en dataset
* `--training-portion` Pourcentage de lignes du dataset à utiliser comme données d'entrainement
* `--generate-report-source` Si fournit, créé un rapport sur les données du dataset au chemin spécifié. (Défaut: ne créé pas de rapport)

# Résultats

Pour notre dataset par défaut (IA antivirus), les meilleurs classifiers sont RandomForest et KNeighbors. On avoisine 99% de réussite.