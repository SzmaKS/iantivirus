from sklearn.svm import SVC


def get_svc(**kwargs):

    return SVC(**kwargs) if len(kwargs) > 0 else SVC(
        C=1.0,
        cache_size=200,
        class_weight=None,
        coef0=0.0,
        decision_function_shape='ovr',
        degree=3,
        gamma='auto',
        kernel='rbf',
        max_iter=-1,
        probability=False,
        random_state=None,
        shrinking=True,
        tol=0.001,
        verbose=False
    )