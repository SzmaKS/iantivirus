from subprocess import call

classifiers = [
    # {
    #     "classifier": "mlp",
    #     "parameters": None
    # },
    # {
    #     "classifier": "qda",
    #     "parameters": None
    # },
    # {
    #     "classifier": "rfc",
    #     "parameters": None
    # },
    {
        "classifier": "kn",
        "parameters": None
    },
    # {
    #     "classifier": "lda",
    #     "parameters": None
    # },
    # {
    #     "classifier": "sgd",
    #     "parameters": None
    # },
    # {
    #     "classifier": "svc",
    #     "parameters": None
    # }
]

cmd = "python3"
script_name = "analyzr.py"

source_file = "resources/antivirus_dataset.csv"
separator = "|"
generate_report_source = None
training_portion = 0.90


def main():
    index = 0
    for classifier in classifiers:
        print("________________________________________________________________________________")
        print("Test n°", index + 1)
        call(
            [
                cmd,
                script_name,
                "--source=" + source_file,
                "--separator=" + separator,
                "--classifier=" + classifiers[index]["classifier"],
                "--training-portion=" + str(training_portion),
                "--classifier-parameters=" + "None" if classifiers[index]["parameters"] is None else classifiers[index]["parameters"],
                "--generate-report-source=" + "None" if generate_report_source is None else generate_report_source
            ],
            shell=False
        )
        print("________________________________________________________________________________")
        index += 1


if __name__ == "__main__":
    main()