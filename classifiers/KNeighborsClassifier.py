from sklearn.neighbors import KNeighborsClassifier


def get_kn(**kwargs):
    return KNeighborsClassifier(**kwargs) if len(kwargs) > 0 else KNeighborsClassifier(
        n_neighbors=2
    )
