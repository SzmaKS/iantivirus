from utils import reader, arg_parser
from utils.arg_parser import parse_classifier_arguments
from utils.numpy_manipulation import keep_lines_by_index, filter_columns_by_index
from utils import ia
from utils import numpy_manipulation
from utils.reader import filter_columns
from utils.reports import generate_report
import pandas_profiling


def main():

    parser = arg_parser.parse_arguments()

    print("Classifier: ", parser.name_of_classifier)

    if parser.classifier_parameters is None or parser.classifier_parameters == "None":
        print("Using default parameters")
    else:
        print(str(parser.classifier_parameters))

    csv_path = parser.source_file
    sep = parser.separator

    df = reader.csv_reader(file_path=csv_path, sep=sep)

    if parser.generate_report_source is not None and parser.generate_report_source != "None":
        generate_report(df, 'Report', parser.generate_report_source)

    ####################################################
    #                                                  #
    # Colonnes sélectionnées après analyse du fichier  #
    # Report.html, généré grâce à panda_profiling      #
    #                                                  #
    ####################################################

    ignore_columns = [
        'MinorSubsystemVersion',
        'MinorImageVersion',
        'SectionsMeanRawsize',
        'SectionsMeanVirtualsize',
        'SectionsMinVirtualsize',
        'SizeOfHeapCommit',
        'SizeOfOptionalHeader',
        'Machine'
    ]

    ######################################################
    # Les colonnes ci-dessus vont être ignorées en plus  #
    # de toutes les colonnes dont le contenu est textuel #
    ######################################################

    df = filter_columns(data_frame=df, columns_to_filter=ignore_columns)

    numpy_before_shuffle = df.to_numpy()
    df_numpy = reader.shuffle(numpy_before_shuffle)

    number_of_trainings = int(float(len(df_numpy)) * parser.training_portion)

    training_rows = numpy_manipulation.keep_lines_by_index(df_numpy, 0, number_of_trainings)

    guessing_rows = numpy_manipulation.keep_lines_by_index(df_numpy, number_of_trainings + 1, len(df) - 1)

    training_set_target = filter_columns_by_index(
        numpy_array=training_rows,
        index=training_rows.shape[1] - 1,
    )

    training_set_data = keep_lines_by_index(
        numpy_array=training_rows,
        start_index=0,
        end_index=len(training_rows),
        remove_last_column=True
    )

    guessing_set_target = filter_columns_by_index(
        numpy_array=guessing_rows,
        index=len(training_rows[1]) - 1,
    )

    guessing_set_data = keep_lines_by_index(
        numpy_array=guessing_rows,
        start_index=0,
        end_index=len(training_rows),
        remove_last_column=True
    )

    print("Initialized")

    if parse_classifier_arguments() is not None:
        ia.get_classifier(
            name_of_classifier=parser.name_of_classifier,
            **parse_classifier_arguments()
        )
    else:
        ia.get_classifier(
            name_of_classifier=parser.name_of_classifier
        )

    ia.data_fit(
        training_set_data,
        training_set_target,
    )

    predict = ia.predict(
        guessing_set_data,
    )

    print("Score: ", ia.get_score(guessing_set_target, predict)*100, "% de bonnes réponses")


if __name__ == "__main__":
    main()
