from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis


def get_qda(**kwargs):
    return QuadraticDiscriminantAnalysis(**kwargs) if len(kwargs) > 0 else QuadraticDiscriminantAnalysis()
