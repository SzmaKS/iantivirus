from sklearn.neural_network import MLPClassifier


def get_mlp(**kwargs):

    return MLPClassifier(**kwargs) if len(kwargs) > 0 else MLPClassifier(
        hidden_layer_sizes=(110,),
        activation='relu',
        solver='lbfgs',
        alpha=0.0001,
        batch_size='auto',
        learning_rate='constant',
        learning_rate_init=0.0001,
        power_t=0.5,
        max_iter=150,
        shuffle=True,
        random_state=None,
        tol=0.0001,
        verbose=False,
        warm_start=False,
        momentum=0.9,
        nesterovs_momentum=True,
        early_stopping=False,
        validation_fraction=0.1,
        beta_1=0.9,
        beta_2=0.999,
        epsilon=1e-08,
        n_iter_no_change=15
    )
